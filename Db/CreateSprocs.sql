USE [KFHSQL];
GO
DROP PROCEDURE [dbo].[USP_Table_0];
GO
DROP PROCEDURE [dbo].[USP_Table_1];
GO
DROP PROCEDURE [dbo].[USP_Table_2];
GO
DROP PROCEDURE [dbo].[USP_Table_3];
GO
DROP PROCEDURE [dbo].[USP_Table_4];
GO
DROP PROCEDURE [dbo].[USP_Table_5];
GO
DROP PROCEDURE [dbo].[USP_Table_6];
GO
DROP PROCEDURE [dbo].[USP_Table_7];
GO
DROP PROCEDURE [dbo].[USP_Table_8];
GO
DROP PROCEDURE [dbo].[USP_Table_9];
GO
DROP PROCEDURE [dbo].[USP_Table_First5];
GO
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

CREATE PROCEDURE [dbo].[USP_Table_0] @Id INT = -1
AS
    BEGIN
        IF @Id >= 0
            SELECT  *
            FROM    Table_0
            WHERE   Id = @Id;
        ELSE
            SELECT  *
            FROM    Table_0;

    END;
GO

CREATE PROCEDURE [dbo].[USP_Table_1] @Id INT = -1
AS
    BEGIN
        IF @Id >= 0
            SELECT  *
            FROM    Table_1
            WHERE   Id = @Id;
        ELSE
            SELECT  *
            FROM    Table_1;

    END;
GO

CREATE PROCEDURE [dbo].[USP_Table_2] @Id INT = -1
AS
    BEGIN
        IF @Id >= 0
            SELECT  *
            FROM    Table_2
            WHERE   Id = @Id;
        ELSE
            SELECT  *
            FROM    Table_2;

    END;
GO

CREATE PROCEDURE [dbo].[USP_Table_3] @Id INT = -1
AS
    BEGIN
        IF @Id >= 0
            SELECT  *
            FROM    Table_3
            WHERE   Id = @Id;
        ELSE
            SELECT  *
            FROM    Table_3;

    END;
GO

CREATE PROCEDURE [dbo].[USP_Table_4] @Id INT = -1
AS
    BEGIN
        IF @Id >= 0
            SELECT  *
            FROM    Table_4
            WHERE   Id = @Id;
        ELSE
            SELECT  *
            FROM    Table_4;

    END;
GO

CREATE PROCEDURE [dbo].[USP_Table_5] @Id INT = -1
AS
    BEGIN
        IF @Id >= 0
            SELECT  *
            FROM    Table_5
            WHERE   Id = @Id;
        ELSE
            SELECT  *
            FROM    Table_5;

    END;
GO

CREATE PROCEDURE [dbo].[USP_Table_6] @Id INT = -1
AS
    BEGIN
        IF @Id >= 0
            SELECT  *
            FROM    Table_6
            WHERE   Id = @Id;
        ELSE
            SELECT  *
            FROM    Table_6;

    END;
GO

CREATE PROCEDURE [dbo].[USP_Table_7] @Id INT = -1
AS
    BEGIN
        IF @Id >= 0
            SELECT  *
            FROM    Table_7
            WHERE   Id = @Id;
        ELSE
            SELECT  *
            FROM    Table_7;

    END;
GO

CREATE PROCEDURE [dbo].[USP_Table_8] @Id INT = -1
AS
    BEGIN
        IF @Id >= 0
            SELECT  *
            FROM    Table_8
            WHERE   Id = @Id;
        ELSE
            SELECT  *
            FROM    Table_8;

    END;
GO

CREATE PROCEDURE [dbo].[USP_Table_9] @Id INT = -1
AS
    BEGIN
        IF @Id >= 0
            SELECT  *
            FROM    Table_9
            WHERE   Id = @Id;
        ELSE
            SELECT  *
            FROM    Table_9;

    END;
GO

CREATE PROCEDURE [dbo].[USP_Table_First5]
AS
    BEGIN
        EXEC dbo.USP_Table_0; 
        EXEC dbo.USP_Table_1;  
        EXEC dbo.USP_Table_2; 
        EXEC dbo.USP_Table_3; 
        EXEC dbo.USP_Table_4;  
    END;
GO