DECLARE @count INT = 0;
WHILE @count < 10000
    BEGIN
        INSERT  INTO Table_0
                ( Id ,
                  Text0 ,
                  Text1 ,
                  Text2 ,
                  Text3 ,
                  Text4 ,
                  Text5 ,
                  Text6 ,
                  Text7 ,
                  Text8 ,
                  Text9
                )
                SELECT  @count ,
                        CHAR(FLOOR(RAND() * 95) + 32) + CHAR(FLOOR(RAND() * 95)
                                                             + 32)
                        + CHAR(FLOOR(RAND() * 95) + 32) ,
                        CHAR(FLOOR(RAND() * 95) + 32) + CHAR(FLOOR(RAND() * 95)
                                                             + 32)
                        + CHAR(FLOOR(RAND() * 95) + 32) ,
                        CHAR(FLOOR(RAND() * 95) + 32) + CHAR(FLOOR(RAND() * 95)
                                                             + 32)
                        + CHAR(FLOOR(RAND() * 95) + 32) ,
                        CHAR(FLOOR(RAND() * 95) + 32) + CHAR(FLOOR(RAND() * 95)
                                                             + 32)
                        + CHAR(FLOOR(RAND() * 95) + 32) ,
                        CHAR(FLOOR(RAND() * 95) + 32) + CHAR(FLOOR(RAND() * 95)
                                                             + 32)
                        + CHAR(FLOOR(RAND() * 95) + 32) ,
                        CHAR(FLOOR(RAND() * 95) + 32) + CHAR(FLOOR(RAND() * 95)
                                                             + 32)
                        + CHAR(FLOOR(RAND() * 95) + 32) ,
                        CHAR(FLOOR(RAND() * 95) + 32) + CHAR(FLOOR(RAND() * 95)
                                                             + 32)
                        + CHAR(FLOOR(RAND() * 95) + 32) ,
                        CHAR(FLOOR(RAND() * 95) + 32) + CHAR(FLOOR(RAND() * 95)
                                                             + 32)
                        + CHAR(FLOOR(RAND() * 95) + 32) ,
                        CHAR(FLOOR(RAND() * 95) + 32) + CHAR(FLOOR(RAND() * 95)
                                                             + 32)
                        + CHAR(FLOOR(RAND() * 95) + 32) ,
                        CHAR(FLOOR(RAND() * 95) + 32) + CHAR(FLOOR(RAND() * 95)
                                                             + 32)
                        + CHAR(FLOOR(RAND() * 95) + 32);
        SET @count = @count + 1;
    END;

INSERT  INTO Table_1
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_0;
INSERT  INTO Table_1
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id + 10000 ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_0;

INSERT  INTO Table_2
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_1;
INSERT  INTO Table_2
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id + 20000 ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_0;

INSERT  INTO Table_3
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_2;
INSERT  INTO Table_3
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id + 30000 ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_0;

INSERT  INTO Table_4
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_3;
INSERT  INTO Table_4
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id + 40000 ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_0;

INSERT  INTO Table_5
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_4;
INSERT  INTO Table_5
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id + 50000 ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_0;

INSERT  INTO Table_6
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_5;
INSERT  INTO Table_6
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id + 60000 ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_0;

INSERT  INTO Table_7
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_6;
INSERT  INTO Table_7
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id + 70000 ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_0;

INSERT  INTO Table_8
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_7;
INSERT  INTO Table_8
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id + 80000 ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_0;

INSERT  INTO Table_9
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_8;
INSERT  INTO Table_9
        ( Id ,
          Text0 ,
          Text1 ,
          Text2 ,
          Text3 ,
          Text4 ,
          Text5 ,
          Text6 ,
          Text7 ,
          Text8 ,
          Text9
        )
        SELECT  Id + 90000 ,
                Text0 ,
                Text1 ,
                Text2 ,
                Text3 ,
                Text4 ,
                Text5 ,
                Text6 ,
                Text7 ,
                Text8 ,
                Text9
        FROM    Table_0;