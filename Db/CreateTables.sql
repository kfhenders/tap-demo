USE KFHSQL;
GO

IF OBJECT_ID(N'Table_0') IS NOT NULL
    BEGIN
        DROP TABLE dbo.Table_0;
    END;
GO
CREATE TABLE Table_0
    (
      Id INT NOT NULL ,
      Text0 VARCHAR(3) NULL ,
      Text1 VARCHAR(3) NULL ,
      Text2 VARCHAR(3) NULL ,
      Text3 VARCHAR(3) NULL ,
      Text4 VARCHAR(3) NULL ,
      Text5 VARCHAR(3) NULL ,
      Text6 VARCHAR(3) NULL ,
      Text7 VARCHAR(3) NULL ,
      Text8 VARCHAR(3) NULL ,
      Text9 VARCHAR(3) NULL
    );
GO
ALTER TABLE dbo.Table_0 ADD CONSTRAINT PK_Table_0 PRIMARY KEY CLUSTERED(Id);
GO

IF OBJECT_ID(N'Table_1') IS NOT NULL
    BEGIN
        DROP TABLE dbo.Table_1;
    END;
GO
CREATE TABLE Table_1
    (
      Id INT NOT NULL ,
      Text0 VARCHAR(3) NULL ,
      Text1 VARCHAR(3) NULL ,
      Text2 VARCHAR(3) NULL ,
      Text3 VARCHAR(3) NULL ,
      Text4 VARCHAR(3) NULL ,
      Text5 VARCHAR(3) NULL ,
      Text6 VARCHAR(3) NULL ,
      Text7 VARCHAR(3) NULL ,
      Text8 VARCHAR(3) NULL ,
      Text9 VARCHAR(3) NULL
    );
GO
ALTER TABLE dbo.Table_1 ADD CONSTRAINT PK_Table_1 PRIMARY KEY CLUSTERED(Id);
GO

IF OBJECT_ID(N'Table_2') IS NOT NULL
    BEGIN
        DROP TABLE dbo.Table_2;
    END;
GO
CREATE TABLE Table_2
    (
      Id INT NOT NULL ,
      Text0 VARCHAR(3) NULL ,
      Text1 VARCHAR(3) NULL ,
      Text2 VARCHAR(3) NULL ,
      Text3 VARCHAR(3) NULL ,
      Text4 VARCHAR(3) NULL ,
      Text5 VARCHAR(3) NULL ,
      Text6 VARCHAR(3) NULL ,
      Text7 VARCHAR(3) NULL ,
      Text8 VARCHAR(3) NULL ,
      Text9 VARCHAR(3) NULL
    );
GO
ALTER TABLE dbo.Table_2 ADD CONSTRAINT PK_Table_2 PRIMARY KEY CLUSTERED(Id);
GO

IF OBJECT_ID(N'Table_3') IS NOT NULL
    BEGIN
        DROP TABLE dbo.Table_3;
    END;
GO
CREATE TABLE Table_3
    (
      Id INT NOT NULL ,
      Text0 VARCHAR(3) NULL ,
      Text1 VARCHAR(3) NULL ,
      Text2 VARCHAR(3) NULL ,
      Text3 VARCHAR(3) NULL ,
      Text4 VARCHAR(3) NULL ,
      Text5 VARCHAR(3) NULL ,
      Text6 VARCHAR(3) NULL ,
      Text7 VARCHAR(3) NULL ,
      Text8 VARCHAR(3) NULL ,
      Text9 VARCHAR(3) NULL
    );
GO
ALTER TABLE dbo.Table_3 ADD CONSTRAINT PK_Table_3 PRIMARY KEY CLUSTERED(Id);
GO

IF OBJECT_ID(N'Table_4') IS NOT NULL
    BEGIN
        DROP TABLE dbo.Table_4;
    END;
GO
CREATE TABLE Table_4
    (
      Id INT NOT NULL ,
      Text0 VARCHAR(3) NULL ,
      Text1 VARCHAR(3) NULL ,
      Text2 VARCHAR(3) NULL ,
      Text3 VARCHAR(3) NULL ,
      Text4 VARCHAR(3) NULL ,
      Text5 VARCHAR(3) NULL ,
      Text6 VARCHAR(3) NULL ,
      Text7 VARCHAR(3) NULL ,
      Text8 VARCHAR(3) NULL ,
      Text9 VARCHAR(3) NULL
    );
GO
ALTER TABLE dbo.Table_4 ADD CONSTRAINT PK_Table_4 PRIMARY KEY CLUSTERED(Id);
GO

IF OBJECT_ID(N'Table_5') IS NOT NULL
    BEGIN
        DROP TABLE dbo.Table_5;
    END;
GO
CREATE TABLE Table_5
    (
      Id INT NOT NULL ,
      Text0 VARCHAR(3) NULL ,
      Text1 VARCHAR(3) NULL ,
      Text2 VARCHAR(3) NULL ,
      Text3 VARCHAR(3) NULL ,
      Text4 VARCHAR(3) NULL ,
      Text5 VARCHAR(3) NULL ,
      Text6 VARCHAR(3) NULL ,
      Text7 VARCHAR(3) NULL ,
      Text8 VARCHAR(3) NULL ,
      Text9 VARCHAR(3) NULL
    );
GO
ALTER TABLE dbo.Table_5 ADD CONSTRAINT PK_Table_5 PRIMARY KEY CLUSTERED(Id);
GO

IF OBJECT_ID(N'Table_6') IS NOT NULL
    BEGIN
        DROP TABLE dbo.Table_6;
    END;
GO
CREATE TABLE Table_6
    (
      Id INT NOT NULL ,
      Text0 VARCHAR(3) NULL ,
      Text1 VARCHAR(3) NULL ,
      Text2 VARCHAR(3) NULL ,
      Text3 VARCHAR(3) NULL ,
      Text4 VARCHAR(3) NULL ,
      Text5 VARCHAR(3) NULL ,
      Text6 VARCHAR(3) NULL ,
      Text7 VARCHAR(3) NULL ,
      Text8 VARCHAR(3) NULL ,
      Text9 VARCHAR(3) NULL
    );
GO
ALTER TABLE dbo.Table_6 ADD CONSTRAINT PK_Table_6 PRIMARY KEY CLUSTERED(Id);
GO

IF OBJECT_ID(N'Table_7') IS NOT NULL
    BEGIN
        DROP TABLE dbo.Table_7;
    END;
GO
CREATE TABLE Table_7
    (
      Id INT NOT NULL ,
      Text0 VARCHAR(3) NULL ,
      Text1 VARCHAR(3) NULL ,
      Text2 VARCHAR(3) NULL ,
      Text3 VARCHAR(3) NULL ,
      Text4 VARCHAR(3) NULL ,
      Text5 VARCHAR(3) NULL ,
      Text6 VARCHAR(3) NULL ,
      Text7 VARCHAR(3) NULL ,
      Text8 VARCHAR(3) NULL ,
      Text9 VARCHAR(3) NULL
    );
GO
ALTER TABLE dbo.Table_7 ADD CONSTRAINT PK_Table_7 PRIMARY KEY CLUSTERED(Id);
GO

IF OBJECT_ID(N'Table_8') IS NOT NULL
    BEGIN
        DROP TABLE dbo.Table_8;
    END;
GO
CREATE TABLE Table_8
    (
      Id INT NOT NULL ,
      Text0 VARCHAR(3) NULL ,
      Text1 VARCHAR(3) NULL ,
      Text2 VARCHAR(3) NULL ,
      Text3 VARCHAR(3) NULL ,
      Text4 VARCHAR(3) NULL ,
      Text5 VARCHAR(3) NULL ,
      Text6 VARCHAR(3) NULL ,
      Text7 VARCHAR(3) NULL ,
      Text8 VARCHAR(3) NULL ,
      Text9 VARCHAR(3) NULL
    );
GO
ALTER TABLE dbo.Table_8 ADD CONSTRAINT PK_Table_8 PRIMARY KEY CLUSTERED(Id);
GO

IF OBJECT_ID(N'Table_9') IS NOT NULL
    BEGIN
        DROP TABLE dbo.Table_9;
    END;
GO
CREATE TABLE Table_9
    (
      Id INT NOT NULL ,
      Text0 VARCHAR(3) NULL ,
      Text1 VARCHAR(3) NULL ,
      Text2 VARCHAR(3) NULL ,
      Text3 VARCHAR(3) NULL ,
      Text4 VARCHAR(3) NULL ,
      Text5 VARCHAR(3) NULL ,
      Text6 VARCHAR(3) NULL ,
      Text7 VARCHAR(3) NULL ,
      Text8 VARCHAR(3) NULL ,
      Text9 VARCHAR(3) NULL
    );
GO
ALTER TABLE dbo.Table_9 ADD CONSTRAINT PK_Table_9 PRIMARY KEY CLUSTERED(Id);
GO