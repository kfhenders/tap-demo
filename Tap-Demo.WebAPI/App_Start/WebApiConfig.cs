﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Mvc;

namespace Tap_Demo.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Only return json
            config.Services.Replace(typeof(IContentNegotiator), new JsonContentNegotiator(new JsonMediaTypeFormatter()));

            // Web API routes
            config.MapHttpAttributeRoutes();


            config.Routes.MapHttpRoute(
                name: "Home",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { controller = "Home", id = UrlParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "UselessApi",
                routeTemplate: "api/{controller}/{howLong}/{returnVal}"
            );

        }
    }
}
