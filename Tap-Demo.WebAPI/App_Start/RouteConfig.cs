﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Tap_Demo.WebAPI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "api/{controller}/{action}/{taskNo}/{tableNo}",
                defaults: new { taskNo = UrlParameter.Optional, tableNo = UrlParameter.Optional  }
            );


        }
    }
}
