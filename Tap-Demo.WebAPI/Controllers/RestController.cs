﻿using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Http;
using TapDemo.Model;

namespace Tap_Demo.WebAPI.Controllers
{
    public class RestController : ApiController
    {

        public int ResetCount()
        {
            RestImplementer restImplementer = new RestImplementer();
            restImplementer.ResetCount();
            return restImplementer.Count;
        }

        public HostingEnvironment GetHostingEnvironment()
        {
            return new HostingEnvironment
            {
                MaxConcurrentRequestsPerCPU = System.Web.Hosting.HostingEnvironment.MaxConcurrentRequestsPerCPU,
                MaxConcurrentThreadsPerCPU = System.Web.Hosting.HostingEnvironment.MaxConcurrentThreadsPerCPU
            };
        }

        public CallStats GetSomeSleep(int taskNo, int tableNo)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            RestImplementer restImplementer = new RestImplementer();
            var stats = restImplementer.SleepAwhile(taskNo, tableNo);

            sw.Stop();
            stats.RequestTimeMs = sw.ElapsedMilliseconds;
            return stats;
        }

        public async Task<CallStats> GetSomeSleepAsync(int taskNo, int tableNo)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            RestImplementer restImplementer = new RestImplementer();
            var stats = await restImplementer.SleepAwhileAsync(taskNo, tableNo).ConfigureAwait(true);

            sw.Stop();
            stats.RequestTimeMs = sw.ElapsedMilliseconds;
            return stats;
        }

    }
}
