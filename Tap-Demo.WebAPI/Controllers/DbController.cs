﻿using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Http;
using TapDemo.Model;

namespace Tap_Demo.WebAPI.Controllers
{
    public class DbController : ApiController
    {

        public int ResetCount()
        {
            DbImplementer dbImplementer = new DbImplementer();
            dbImplementer.ResetCount();
            return dbImplementer.Count;
        }

        public CallStats GetQuick(int taskNo, int tableNo)
        {
            
            Stopwatch sw = new Stopwatch();
            sw.Start();

            DbImplementer dBImplementer = new DbImplementer();
            var stats =  dBImplementer.QuickDbCall(taskNo, tableNo);

            sw.Stop();
            stats.RequestTimeMs = sw.ElapsedMilliseconds;
            return stats;
        }

        public CallStats GetLong(int taskNo, int tableNo)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            DbImplementer dBImplementer = new DbImplementer();
            var stats = dBImplementer.LongDbCall(taskNo, tableNo);

            sw.Stop();
            stats.RequestTimeMs = sw.ElapsedMilliseconds;
            return stats;
        }

        public async Task<CallStats> GetLongAsync(int taskNo, int tableNo)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            DbImplementer dBImplementer = new DbImplementer();
            var stats = await dBImplementer.LongAsyncDbCall(taskNo, tableNo).ConfigureAwait(false);

            sw.Stop();
            stats.RequestTimeMs = sw.ElapsedMilliseconds;
            return stats;
        }

        public CallStats GetFirst5(int taskNo)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            DbImplementer dBImplementer = new DbImplementer();
            var stats = dBImplementer.GetFirst5(taskNo);

            sw.Stop();
            stats.RequestTimeMs = sw.ElapsedMilliseconds;
            return stats;
        }

        public async Task<CallStats> GetFirst5Async(int taskNo)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            DbImplementer dBImplementer = new DbImplementer();
            var stats = await dBImplementer.GetFirst5Async(taskNo).ConfigureAwait(false);

            sw.Stop();
            stats.RequestTimeMs = sw.ElapsedMilliseconds;
            return stats;
        }

        public CallStats GetFirst5ByPieces(int taskNo)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            DbImplementer dBImplementer = new DbImplementer();
            var stats = dBImplementer.GetFirst5ByPieces(taskNo);

            sw.Stop();
            stats.RequestTimeMs = sw.ElapsedMilliseconds;
            return stats;
        }
    }
    
}
