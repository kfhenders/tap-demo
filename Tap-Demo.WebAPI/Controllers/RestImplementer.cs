﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using TapDemo.Model;
namespace Tap_Demo.WebAPI.Controllers
{
    public class RestImplementer
    {
        static int _count = -1;
        static object _lockObj = new object();

        public int Count
        {
            get
            {
                return _count;
            }
        }

        public void ResetCount()
        {
            Interlocked.Exchange(ref _count, -1);
        }

        public CallStats SleepAwhile(int taskNo, int sleepTimeMs)
        {

            Stopwatch sw = new Stopwatch();
            sw.Start();

            var callOrder = Interlocked.Increment(ref _count);
            CallStats stats = new CallStats
            {
                MethodCalled = "Sleep",
                TaskNo = taskNo,
                RequestNo = callOrder
            };

            // Don't actually sleep as that yields its slice of processor time and doesn't simulate real work
            DateTime exitAt = DateTime.Now.AddMilliseconds(sleepTimeMs);
            while (DateTime.Now < exitAt)
            {
                Thread.SpinWait(10);
            }

            sw.Stop();
            stats.RequestTimeMs = sw.ElapsedMilliseconds;
            return stats;
        }

        public async Task<CallStats> SleepAwhileAsync(int taskNo, int sleepTimeMs)
        {

            Stopwatch sw = new Stopwatch();
            sw.Start();

            var callOrder = Interlocked.Increment(ref _count);
            CallStats stats = new CallStats
            {
                MethodCalled = "Sleep",
                TaskNo = taskNo,
                RequestNo = callOrder
            };

            await Task.Delay(sleepTimeMs).ConfigureAwait(false);

            sw.Stop();
            stats.RequestTimeMs = sw.ElapsedMilliseconds;
            return stats;
        }

    }
}