﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TapDemo.Model;

namespace Tap_Demo.WebAPI.Controllers
{
    public class DbImplementer
    {
        // Our basic Azure SQL Db has a maximum request count of 30 ...
        static SemaphoreSlim _gate = new SemaphoreSlim(30, 30);
        static int _count = -1;
        static object _lockObj = new object();

        static string ConnString = ConfigurationManager.ConnectionStrings["MyConn"].ConnectionString;

        const string dbSprocFormat = "dbo.USP_Table_{0}";

        public int Count
        {
            get
            {
                return _count;
            }
        }

        public void ResetCount()
        {
            Interlocked.Exchange(ref _count, -1);
        }


        public CallStats LongDbCall(int taskNo, int tableNo)
        {

            var callOrder = Interlocked.Increment(ref _count);
            CallStats stats = new CallStats
            {
                MethodCalled = "Long",
                TaskNo = taskNo,
                RequestNo = callOrder
            };
            _gate.Wait();
            try
            {
                using (var connection = new SqlConnection(ConnString))
                {
                    connection.Open();
                    string sql = String.Format(dbSprocFormat, tableNo);

                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 120;
                        using (var reader = command.ExecuteReader())
                        {
                            IEnumerable<Junk> rows = ReadRecords(reader);
                            stats.RecordsRead = rows.Count();
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                stats.ExceptionMessage = ex.Message;
            }
            finally
            {
                _gate.Release();
            }

            return stats;

        }

        public async Task<CallStats> LongAsyncDbCall(int taskNo, int tableNo, bool incrementCount = true)
        {

            var callOrder = incrementCount ? Interlocked.Increment(ref _count) : -1;
            CallStats stats = new CallStats
            {
                MethodCalled = "Long Async",
                TaskNo = taskNo,
                RequestNo = callOrder
            };
            await _gate.WaitAsync().ConfigureAwait(false);
            try
            {
                using (var connection = new SqlConnection(ConnString))
                {
                    await connection.OpenAsync().ConfigureAwait(false);
                    string sql = String.Format(dbSprocFormat, tableNo);

                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 120;
                        using (var reader = await command.ExecuteReaderAsync().ConfigureAwait(false))
                        {
                            IEnumerable<Junk> rows = await ReadRecordsAsync(reader).ConfigureAwait(false);
                            stats.RecordsRead = rows.Count();
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                stats.ExceptionMessage = ex.Message;
            }
            finally
            {
                _gate.Release();
            }

            return stats;

        }

        public CallStats QuickDbCall(int taskNo, int tableNo)
        {

            var callOrder = Interlocked.Increment(ref _count);
            CallStats stats = new CallStats
            {
                MethodCalled = "Quick",
                TaskNo = taskNo,
                RequestNo = callOrder
            };
            _gate.Wait();
            try
            {
                using (var connection = new SqlConnection(ConnString))
                {
                    connection.Open();

                    string sql = String.Format(dbSprocFormat, tableNo);
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("Id", taskNo);
                        using (var reader = command.ExecuteReader())
                        {
                            IEnumerable<Junk> rows = ReadRecords(reader);
                            stats.RecordsRead = rows.Count();
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                stats.ExceptionMessage = ex.Message;
            }
            finally
            {
                _gate.Release();
            }

            return stats;

        }

        public CallStats GetFirst5(int taskNo)
        {
            var callOrder = Interlocked.Increment(ref _count);
            CallStats stats = new CallStats
            {
                MethodCalled = "First5",
                TaskNo = taskNo,
                RequestNo = callOrder
            };
            _gate.Wait();
            try
            {
                using (var connection = new SqlConnection(ConnString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("dbo.USP_Table_First5", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 120;
                        using (var reader = command.ExecuteReader())
                        {
                            IEnumerable<Junk> rows = ReadRecords(reader);
                            while (reader.NextResult())
                            {
                                rows = rows.Union(ReadRecords(reader));
                            }
                            stats.RecordsRead = rows.Count();
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                stats.ExceptionMessage = ex.Message;
            }
            finally
            {
                _gate.Release();
            }

            return stats;
        }

        public async Task<CallStats> GetFirst5Async(int taskNo)
        {
            var callOrder = Interlocked.Increment(ref _count);
            CallStats stats = new CallStats
            {
                MethodCalled = "First5Async",
                TaskNo = taskNo,
                RequestNo = callOrder
            };
            await _gate.WaitAsync().ConfigureAwait(false);
            try
            {
                using (var connection = new SqlConnection(ConnString))
                {
                    await connection.OpenAsync().ConfigureAwait(false);

                    using (var command = new SqlCommand("dbo.USP_Table_First5", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = 120;
                        using (var reader = await command.ExecuteReaderAsync().ConfigureAwait(false))
                        {
                            IEnumerable<Junk> rows = await ReadRecordsAsync(reader).ConfigureAwait(false);
                            while (await reader.NextResultAsync().ConfigureAwait(false))
                            {
                                rows = rows.Union(await ReadRecordsAsync(reader).ConfigureAwait(false));
                            }
                            stats.RecordsRead = rows.Count();
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                stats.ExceptionMessage = ex.Message;
            }
            finally
            {
                _gate.Release();
            }

            return stats;
        }

        public CallStats GetFirst5ByPieces(int taskNo)
        {
            int tableCount = 5;

            var callOrder = Interlocked.Increment(ref _count);
            CallStats totalStats = new CallStats
            {
                MethodCalled = "First5ByPieces",
                TaskNo = taskNo,
                RequestNo = callOrder
            };

            Action<CallStats> sumCallStats = (cs) =>
            {
                lock (_lockObj)
                {
                    totalStats.RecordsRead += cs.RecordsRead;
                    totalStats.RequestTimeMs += cs.RequestTimeMs;
                }
            };

            Task[] tasks = new Task[tableCount];
            for (int i=0;i< tableCount; i++)
            {
                var currIndex = i;
                tasks[currIndex] = Task.Run(async () => await LongAsyncDbCall(currIndex, currIndex, false).ConfigureAwait(false)).ContinueWith(r => sumCallStats(r.Result));
            }

            Task.WaitAll(tasks);
            return totalStats;
        }

        private IEnumerable<Junk> ReadRecords(IDataReader reader)
        {
            List<Junk> rows = new List<Junk>();
            while (reader.Read())
            {
                rows.Add(new Junk
                {
                    Id = Convert.ToInt32(reader["Id"]),
                    Text0 = reader["Text0"].ToString(),
                    Text1 = reader["Text1"].ToString(),
                    Text2 = reader["Text2"].ToString(),
                    Text3 = reader["Text3"].ToString(),
                    Text4 = reader["Text4"].ToString(),
                    Text5 = reader["Text5"].ToString(),
                    Text6 = reader["Text6"].ToString(),
                    Text7 = reader["Text7"].ToString(),
                    Text8 = reader["Text8"].ToString(),
                    Text9 = reader["Text9"].ToString()

                });
            }
            return rows;
        }

        private async Task<IEnumerable<Junk>> ReadRecordsAsync(SqlDataReader reader)
        {
            List<Junk> rows = new List<Junk>();
            while (await reader.ReadAsync().ConfigureAwait(false))
            {
                rows.Add(new Junk
                {
                    Id = Convert.ToInt32(reader["Id"]),
                    Text0 = reader["Text0"].ToString(),
                    Text1 = reader["Text1"].ToString(),
                    Text2 = reader["Text2"].ToString(),
                    Text3 = reader["Text3"].ToString(),
                    Text4 = reader["Text4"].ToString(),
                    Text5 = reader["Text5"].ToString(),
                    Text6 = reader["Text6"].ToString(),
                    Text7 = reader["Text7"].ToString(),
                    Text8 = reader["Text8"].ToString(),
                    Text9 = reader["Text9"].ToString()

                });
            }
            return rows;
        }
    }
}