﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TapDemo.Model
{
    public static class CallCompletionStats
    {
        static int _succcesses = 0;
        static int _failures = 0;
        static int _retries = 0;
        static int _total = 0;
        static DateTime _startTime = DateTime.Now;

        public static void Reset()
        {
            Interlocked.Exchange(ref _succcesses, 0);
            Interlocked.Exchange(ref _failures, 0);
            Interlocked.Exchange(ref _retries, 0);
            Interlocked.Exchange(ref _total, 0);
            _startTime = DateTime.Now;
        }

        public static int Successes
        {
            get { return _succcesses; }
        }

        public static int Failures
        {
            get { return _failures; }
        }

        public static int Retries
        {
            get { return _retries; }
        }

        public static int Total
        {
            get { return _total; }
        }

        public static int ElapsedSeconds
        {
            get { return (int)(DateTime.Now - _startTime).TotalSeconds; }
        }

        public static int CallsPerSecond
        {
            get
            {
                return ElapsedSeconds == 0 ? 0 : (int)(Total / ElapsedSeconds);
            }
        }

        public static int AddSuccess()
        {
            IncrementTotal();
            return Interlocked.Increment(ref _succcesses);
        }

        public static int AddFailure(int statusCode)
        {
            IncrementTotal();
            return Interlocked.Increment(ref _failures);
        }

        public static int AddRetry()
        {
            IncrementTotal();
            return Interlocked.Increment(ref _retries);
        }

        private static void IncrementTotal()
        {
            Interlocked.Increment(ref _total);
        }

        public static IEnumerable<string> GetHeader()
        {
            return new String[]
            {
                "Elapsed(sec)  Total Calls  Successes  Failures  Retries  Calls/Sec",
                "------------------------------------------------------------------"
            };
        }


        public static string CurrentStats()
        {
            return ElapsedSeconds.ToString("N0").PadLeft(12) +
                    Total.ToString("N0").PadLeft(13) +
                    Successes.ToString("N0").PadLeft(11) +
                    Failures.ToString("N0").PadLeft(10) +
                    Retries.ToString("N0").PadLeft(9) +
                    CallsPerSecond.ToString("N0").PadLeft(11);

        }

    }
}
