﻿using Newtonsoft.Json;
using System;

namespace TapDemo.Model
{
    public class HostingEnvironment
    {
        public int MaxConcurrentRequestsPerCPU { get; set; }
        public int MaxConcurrentThreadsPerCPU { get; set; }

        public HostingEnvironment() { }

        public HostingEnvironment(string json) : this()
        {
            var he = JsonConvert.DeserializeObject<HostingEnvironment>(json);
            this.MaxConcurrentRequestsPerCPU = he.MaxConcurrentRequestsPerCPU;
            this.MaxConcurrentThreadsPerCPU = he.MaxConcurrentThreadsPerCPU;
        }

        public override string ToString()
        {
            return String.Format("MaxConcurrentRequestsPerCPU: {0}; MaxConcurrentThreadsPerCPU: {1}", MaxConcurrentRequestsPerCPU, MaxConcurrentThreadsPerCPU);
        }
    }
}
