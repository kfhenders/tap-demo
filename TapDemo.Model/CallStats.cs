﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TapDemo.Model
{
    public class CallStats 
    {
        public string MethodCalled { get; set; }
        public int TaskNo { get; set; }
        public long TaskTimeMs { get; set; }
        public int RequestNo { get; set; }
        public long RequestTimeMs { get; set; }
        public long QueueTime { get { return TaskTimeMs - RequestTimeMs; } }
        public int RecordsRead { get; set; }
        public string ExceptionMessage { get; set; }

        public CallStats()
        {

        }


        public CallStats(string json) : this()
        {

            var cs = JsonConvert.DeserializeObject<CallStats>(json);
            this.MethodCalled = cs.MethodCalled;
            this.TaskNo = cs.TaskNo;
            this.TaskTimeMs = cs.TaskTimeMs;
            this.RequestNo = cs.RequestNo;
            this.RequestTimeMs = cs.RequestTimeMs;
            this.RecordsRead = cs.RecordsRead;
            this.ExceptionMessage = cs.ExceptionMessage;
        }

        public static IEnumerable<string> GetHeader()
        {
            return new String[]
            {
                "MethodCalled     TaskNo  TaskTimeMs  RequestNo  RequestTimeMs  QueueTime  RecordsRead  ExceptionMessage",
                "-------------------------------------------------------------------------------------------------------"             
            };
        }

        public override string ToString()
        {
            return MethodCalled.PadRight(15) +
                    TaskNo.ToString("N0").PadLeft(8) +
                    TaskTimeMs.ToString("N0").PadLeft(12) +
                    RequestNo.ToString("N0").PadLeft(11) +
                    RequestTimeMs.ToString("N0").PadLeft(15) +
                    QueueTime.ToString("N0").PadLeft(11) +
                    RecordsRead.ToString("N0").PadLeft(13) +
                    "  " +
                    ExceptionMessage;
        }

        
    }
}
