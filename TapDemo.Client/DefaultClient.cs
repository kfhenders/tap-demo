﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TapDemo.Client
{
    public class DefaultClient
    {
        public async Task DoStuff()
        {

            await Do1();
            Console.WriteLine();
            await Do2();
            Console.WriteLine();
            Do3();

            Console.ReadLine();
        }

        private async Task Do1()
        {
            Console.WriteLine("Method Do1");
            Console.WriteLine();

            var totTime = await Wait(800).ConfigureAwait(false);
            Console.WriteLine($"Wait method returned {totTime}");
        }

        private async Task Do2()
        {

            Console.WriteLine("Method Do2");
            Console.WriteLine();

            var waitTask = Wait(800).ConfigureAwait(false);

            Console.WriteLine("I can do work here if I want ...");

            var totTime = await waitTask;
            Console.WriteLine($"Wait method returned {totTime}");
        }

        private void Do3()
        {

            Console.WriteLine("Method Do3");
            Console.WriteLine();

            long totalTime = 0;

            Action<Task<long>> onWaited = (l) =>
            {
                Console.WriteLine($"Wait method returned {l.Result}");
                Interlocked.Add(ref totalTime, l.Result);
            };

            List<Task> waitTasks = new List<Task>();
            for (int i=0;i<1000;i+=100)
            {
                waitTasks.Add(Wait(i).ContinueWith(onWaited));
            }

            for (int i=10; i<100; i+=10)
            {
                Thread.Sleep(i);
                Console.WriteLine("Doing very important things right now ...");
            }

            Task.WaitAll(waitTasks.ToArray());

            Console.WriteLine($"Waited for a total of {totalTime} milliseconds");
           

        }

        private async Task<long> Wait(int ms)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Console.WriteLine($"Wait method will wait {ms} milliseconds!");

            await Task.Delay(ms).ConfigureAwait(false);

            sw.Stop();
            return sw.ElapsedMilliseconds;
        }


    }
}
