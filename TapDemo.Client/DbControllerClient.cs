﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using TapDemo.Model;

namespace TapDemo.Client
{
    public class DbControllerClient
    {
        static string _rootUrl = ConfigurationManager.AppSettings["RootUrl"];
        static string _resetCountDbUrl = ConfigurationManager.AppSettings["RootUrl"] + "/api/db/ResetCount";
        static int _tableCount = int.Parse(ConfigurationManager.AppSettings["TableCount"]);
        static int _taskCounter = -1;

        static object _lockObj = new object();
        static CallStats _totalStats;

        public DbControllerClient() { }

        public async Task AsyncVsStandard(int longCalls, int quickCalls)
        {
            await AsyncVsStandard(false, longCalls, quickCalls).ConfigureAwait(false);
            await AsyncVsStandard(true, longCalls, quickCalls).ConfigureAwait(false);
        }

        public async Task GetFirst5Async(int reps, bool async)
        {
            await ResetAndWarmUp().ConfigureAwait(false);

            var singleCallMethod = async ? "GetFirst5Async" : "GetFirst5";
            var piecesCallMethod = "GetFirst5ByPieces";
            var urlFormat = _rootUrl + "/api/db/{0}/{1}/0";

            Interlocked.Exchange(ref _taskCounter, -1);
            ResetCount(_resetCountDbUrl);

            WriteHeader();
            Action<CallStats> writeCallStats = (cs) =>
            {
                Console.WriteLine(cs);
            };

            Task[] tasks = new Task[reps * 2];
            for (int i = 0; i < reps; i++)
            {
                var singleCallTaskNo = Interlocked.Increment(ref _taskCounter);
                var singleCallUrl = String.Format(urlFormat, singleCallMethod, singleCallTaskNo);
                tasks[singleCallTaskNo] = Task.Run(async () => await GetWebRequest(singleCallUrl).ConfigureAwait(false)).ContinueWith(r => writeCallStats(r.Result));

                var piecesCallTaskNo = Interlocked.Increment(ref _taskCounter);
                var piecesCallUrl = String.Format(urlFormat, piecesCallMethod, piecesCallTaskNo);
                tasks[piecesCallTaskNo] = Task.Run(async () => await GetWebRequest(piecesCallUrl).ConfigureAwait(false)).ContinueWith(r => writeCallStats(r.Result));
            }

            Task.WaitAll(tasks);

        }

        public static async Task ResetAndWarmUp()
        {
            Console.WriteLine("Resetting IIS ...");
            Process iisReset = new Process();
            iisReset.StartInfo.FileName = @"C:\Windows\System32\iisreset.exe";
            iisReset.Start();
            iisReset.WaitForExit();

            Console.WriteLine("Warming up ...");
            var urlFormat = _rootUrl + "/api/db/GetQuick/0/{0}";
            var randomizer = new Random();
            for (int i=0; i<30; i++)
            {
                var url = String.Format(urlFormat, randomizer.Next(_tableCount));
                await GetWebRequest(url).ConfigureAwait(false);
            }

            HostingEnvironment env = await RestControllerClient.GetHostingEnvironment().ConfigureAwait(false);
            Console.WriteLine();
            Console.WriteLine(env);
            
        }

        private async Task AsyncVsStandard(bool async, int longCalls, int quickCalls)
        {

            await ResetAndWarmUp().ConfigureAwait(false);

            var longRequestMethod = async ? "GetLongAsync" : "GetLong";
            var shortRequestMethod = "GetQuick";
            var urlFormat = _rootUrl + "/api/db/{0}/{1}/{2}";
            var randomizer = new Random(123456);

            _totalStats = new CallStats
            {
                MethodCalled = "TOTAL",
                RequestNo = -1,
                TaskNo = -1
            };


            Interlocked.Exchange(ref _taskCounter, -1);
            ResetCount(_resetCountDbUrl);

            WriteHeader();
            Action<CallStats> writeCallStats = (cs) =>
            {
                Console.WriteLine(cs);
                lock(_lockObj)
                {
                    _totalStats.RecordsRead += cs.RecordsRead;
                    _totalStats.RequestTimeMs += cs.RequestTimeMs;
                    _totalStats.TaskTimeMs += cs.TaskTimeMs;
                }
            };

            Stopwatch sw = new Stopwatch();
            sw.Start();

            Task[] tasks = new Task[longCalls + quickCalls];
            for (int i = 0; i < longCalls; i++)
            {
                var taskNo = Interlocked.Increment(ref _taskCounter);
                var url = String.Format(urlFormat, longRequestMethod, taskNo, randomizer.Next(_tableCount));
                tasks[taskNo] = Task.Run(async () => await GetWebRequest(url).ConfigureAwait(false)).ContinueWith(r => writeCallStats(r.Result));
            }

            for (int i = 0; i < quickCalls; i++)
            {
                var taskNo = Interlocked.Increment(ref _taskCounter);
                var url = String.Format(urlFormat, shortRequestMethod, taskNo, randomizer.Next(_tableCount));
                tasks[taskNo] = Task.Run(async () => await GetWebRequest(url).ConfigureAwait(false)).ContinueWith(r => writeCallStats(r.Result));
            }

            try
            {
                Task.WaitAll(tasks, -1);
            }
            catch (AggregateException ae)
            {
                foreach (var e in ae.InnerExceptions)
                {
                    Console.WriteLine(e.Message);
                }
            }

            sw.Stop();

            Console.WriteLine();
            Console.WriteLine("{0}{1} seconds", _totalStats, Math.Floor(sw.ElapsedMilliseconds / 1000m).ToString("N0"));
            Console.WriteLine();
            Console.WriteLine();

        }

        private static void WriteHeader()
        {
            Console.WriteLine();
            Console.WriteLine();
            IEnumerable<string> headerRows = CallStats.GetHeader();
            foreach (var line in headerRows)
            {
                Console.WriteLine(line);
            }
        }

        private static void ResetCount(string url)
        {
            var request = HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentLength = 0;
            var webResponse = request.GetResponse();
        }

        private static async Task<CallStats> GetWebRequest(string url)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string response;
            var request = HttpWebRequest.Create(url);
            var webResponse = await request.GetResponseAsync().ConfigureAwait(false);

            using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
            {
                response = await reader.ReadToEndAsync().ConfigureAwait(false);
            }

            sw.Stop();
            CallStats stats = new CallStats(response);
            stats.TaskTimeMs = sw.ElapsedMilliseconds;

            return stats;
        }
    }

}

