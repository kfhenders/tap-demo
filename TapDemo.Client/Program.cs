﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using TapDemo.Model;

namespace TapDemo.Client
{
    class Program
    {

        static void Main(string[] args)
        {

            ServicePointManager.Expect100Continue = false;
            ServicePointManager.UseNagleAlgorithm = false;
            ServicePointManager.DefaultConnectionLimit = int.MaxValue;



            switch (args[0].ToLower())
            {
                case "dbasync":
                    ExecuteDbAsyncVsStandard(args);
                    break;
                case "dbfirst5":
                    ExecuteDbGetFirst5(args);
                    break;
                case "lots":
                    ExecuteLots(args);
                    break;
                default:
                    Default(args);
                    break;
            }

        }

        private static void Default(string[] args)
        {
            var client = new DefaultClient();
            client.DoStuff().Wait();
        }

        private static void ExecuteDbAsyncVsStandard(string[] args)
        {
            DbControllerClient client = new DbControllerClient();
            Task.Run(async () => await client.AsyncVsStandard(int.Parse(args[1]), int.Parse(args[2])).ConfigureAwait(false)).Wait();
        }

        private static void ExecuteDbGetFirst5(string[] args)
        {
            DbControllerClient client = new DbControllerClient();
            Task.Run(async () => await client.GetFirst5Async(int.Parse(args[1]), bool.Parse(args[2])).ConfigureAwait(false)).Wait();
        }

        private static void ExecuteLots(string[] args)
        {
            RestControllerClient client = new RestControllerClient();
            Task.Run(async () => await client.MakeLotsOfCalls(int.Parse(args[1]), int.Parse(args[2])).ConfigureAwait(false)).Wait();
        }
    }
}
