﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TapDemo.Model;
using Polly;
using System.Web;

namespace TapDemo.Client
{
    public class RestControllerClient
    {
        static string _rootUrl = ConfigurationManager.AppSettings["RootUrl"];
        static string _resetCountDbUrl = ConfigurationManager.AppSettings["RootUrl"] + "/api/rest/ResetCount";
        static string _hostingEnvironmentUrl = ConfigurationManager.AppSettings["RootUrl"] + "/api/rest/GetHostingEnvironment";
        static string _sleepAWhileUrlFormat = ConfigurationManager.AppSettings["RootUrl"] + "/api/rest/{0}/{1}/{2}";
        static int _taskCounter = 0;

        public static async Task<HostingEnvironment> GetHostingEnvironment()
        {
            string result = await GetWebRequest(_hostingEnvironmentUrl, CancellationToken.None).ConfigureAwait(false);
            return new HostingEnvironment(result);
        }

        public async Task MakeLotsOfCalls(int howMany, int maxSleepMs)
        {
            await MakeLotsOfCalls(true, howMany, maxSleepMs).ConfigureAwait(false);
            await MakeLotsOfCalls(false, howMany, maxSleepMs).ConfigureAwait(false);
        }

        private async Task MakeLotsOfCalls(bool async, int howMany, int maxSleepMs)
        {
            await DbControllerClient.ResetAndWarmUp().ConfigureAwait(false);
            CallCompletionStats.Reset();

            Interlocked.Exchange(ref _taskCounter, 0);
            ResetCount(_resetCountDbUrl);
            CancellationTokenSource tokenSource = new CancellationTokenSource();

            WriteHeader(async);

            Task[] tasks = new Task[howMany];
            var randomizer = new Random(123456);
            for (int i = 0; i < howMany; i++)
            {
                var url = String.Format(_sleepAWhileUrlFormat, async ? "GetSomeSleepAsync" : "GetSomeSleep", _taskCounter, randomizer.Next(maxSleepMs));
                tasks[_taskCounter] = Task.Run(async () => await GetWebRequest(url, tokenSource.Token).ConfigureAwait(false));
                Interlocked.Increment(ref _taskCounter);
            }

            Task loggingTask = LogForSanity(tokenSource.Token);

            Task.WaitAll(tasks, 60000);
            tokenSource.Cancel();

            var nonRunTasks = (from t in tasks where t.Status != TaskStatus.RanToCompletion select t).ToList();

            Console.WriteLine(CallCompletionStats.CurrentStats());
            Console.WriteLine();
            Console.WriteLine();
        }

        private static async Task LogForSanity(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                await Task.Delay(15000, token);
                if (!token.IsCancellationRequested)
                {
                    Console.WriteLine(CallCompletionStats.CurrentStats());
                }
            }
        }

        private static void ResetCount(string url)
        {
            var request = HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentLength = 0;
            var webResponse = request.GetResponse();
        }

        private static void WriteHeader(bool async)
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("async={0}", async);
            IEnumerable<string> headerRows = CallCompletionStats.GetHeader();
            foreach (var line in headerRows)
            {
                Console.WriteLine(line);
            }
        }

        private static async Task<string> GetWebRequest(string url, CancellationToken token)
        {
            string response = String.Empty;
            int statusCode = 0;

            var policy = Policy.Handle<WebException>(we =>
            {
                return we.Message != null && we.Message.Contains("(503)");

            }).WaitAndRetryAsync(10, (retryNo) =>
            {
                double waitSeconds = Math.Min(Math.Pow(2, retryNo), 16);
                return TimeSpan.FromSeconds(waitSeconds);
            }, (e, ts) =>
            {
                //Console.WriteLine("Waited: {0}: Got: {1}", ts.TotalSeconds, e.Message);
                CallCompletionStats.AddRetry();
            });

            try
            {
                var policyResult = await policy.ExecuteAndCaptureAsync(async () => await HttpWebRequest.Create(url).GetResponseAsync(), token).ConfigureAwait(false);
                if (token.IsCancellationRequested)
                {
                    return response;
                }
                if (policyResult.Outcome == OutcomeType.Successful)
                {
                    statusCode = 200;
                    using (StreamReader reader = new StreamReader(policyResult.Result.GetResponseStream()))
                    {
                        response = await reader.ReadToEndAsync().ConfigureAwait(false);
                    }
                }
            }
            catch (WebException we)
            {

                response = we.Message;
                HttpWebResponse webResponse = we.Response as HttpWebResponse;
                if (webResponse != null)
                {
                    statusCode = (int)((HttpWebResponse)we.Response).StatusCode;
                }

            }
            if (statusCode == 200)
            {
                CallCompletionStats.AddSuccess();
            }
            else
            {
                CallCompletionStats.AddFailure(statusCode);
            }
            return response;
        }
    }
}
